# Maanmittauslaitokset karttalehtijaon ruudukkojen ruudut, jotka osuvat Suomen alueelle

Eli Maanmittauslaitoksen karttalehtijaon ruudukkojen ruudut, jotka leikkaavat
Suomen ulkorajan. Leikkauksessa käytetty valtakunnanraja on Maanmittauslaitoksen
Kuntajako (2019) aineistosta.

## Avoimien aineistojen käyttämät ruudukot

| Tietotuote                  | Ruudukko    |
| --------------------------- | ----------- |
| Kiinteistörekisterikartta   | utm10       |
| Korkeusmalli (2m)           | utm10       |
| Korkeusmalli (10m)          | utm25       |
| Laserkeilausaineisto        | utm5        |
| Maastotietokanta (kaikki)   | utm25LR     |
| Maastotietokanta (tiestö)   | utm100      |


Aineistolähde: Maanmittauslaitos 2019, Avoimien aineistojen lisenssi.
